export interface IReservation {
	from: Date
	to: Date
}

export interface IRoom {
	name: string;
	description: string;
	capacity: number;
	equipements: Object[];
	reservations: IReservation[],
	createdAt: Date;
	updatedAt: Date;
}