import { ROOM_API_URL } from '../constants';
import { IRoom } from '../models/IRoom';

export class RoomConnector {
	public static async findAll({ from, to }: any = {}) {
		console.log({ from, to });
		console.log(JSON.stringify({ from, to }));
		const options = {
			method: 'POST',
			headers: {
				'content-type': 'application/json;charset=UTF-8',
			},
			body: JSON.stringify({ from, to }),
		};
		console.log(options);
		const response = await fetch(`${ ROOM_API_URL }/rooms/all`, options);


		if (response.ok) {
			const rooms = await response.json() as IRoom;
			if (rooms) {
				return rooms;
			} else {
				return Promise.reject(new Error(`No rooms with available "${ rooms }"`));
			}
		} else {
			const { error } = await response.json();
			// handle the graphql errors
			console.error('[findAll] Something gone wrong fetch meeting\'s room ', { error });
			const custom = new Error('Something gone wrong fetch meeting\'s room ');
			return Promise.reject(error ?? custom.message);
		}

	}
}