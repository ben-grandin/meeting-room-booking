import { Document, Model } from 'mongoose';

export class AbstractService<T> {

	constructor(private document: Model<T>) {
	}

	public findAll(): Promise<T[]> {
		return this.document.find({}).exec();
	}

	public async find(id: string): Promise<T> {
		const doc = await this.document.findById(id).exec();

		if (!doc) throw new Error(`Model with id '${ id }' not found'`);

		return doc;
	}

	public create(doc: T): Promise<Document> {
		const newDoc = new this.document(doc);
		return newDoc.save();
	}

	public async update(id: string, doc: T): Promise<T> {
		const updatedDoc = await this.document.findByIdAndUpdate(id, doc).exec();

		if (!updatedDoc) throw new Error(`Doc with id '${ id }' not found'`);

		return updatedDoc;
	}

	public async delete(id: string): Promise<T> {
		const deletedDoc = await this.document.findByIdAndDelete(id).exec();

		if (!deletedDoc) throw new Error(`Doc with id '${ id }' not found'`);

		return deletedDoc;
	}
}

// TODO : implement AbstractService with IDataService
// export interface IDataService<T> {
//
// 	all(): Promise<T[]>
//
// 	// paginate(page: number, relations?: string[]): Promise<IPaginatedResult<T>>
//
// 	create(body: T): Promise<T>
//
// 	findOne(condition: object, relations?: string[]): Promise<T>
//
// 	update(id: number, body: T): Promise<T>
//
// 	delete(id: number): Promise<T>
// }