import { IRoom, Room } from '../model/room.model';
import moment from 'moment/moment';

export class RoomService {
	// TODO : #5 filter reservation in DB
	// const temp = {
	// 	reservations: {
	// 		$elemMatch: {
	// 			from: { $gte: 8 },
	// 			to: { $lte: 8 },
	// 		},
	// 	},
	// }
	public findAll({ from, to, ...condition }: any = {}): Promise<IRoom[]> {
		return new Promise(async (resolve, reject) => {
			try {

				const rooms = await Room.find(condition).exec();
				if (!from && !to) resolve(rooms);
				console.log(from, to);
				if (from) {
					if (moment(from, true).isValid()) {
						from = new Date(from).getTime();
					} else {
						reject(new Error(`From format incorrect ${ from }`));
					}
				}

				if (to) {
					if (moment(to, true).isValid()) {
						to = new Date(to).getTime();
					} else {
						reject(new Error(`To format incorrect ${ to }`));
					}
				}

				if ((from && !to) || (!from && to)) reject(new Error('From and To must be both set or both undefined'));
				if (from > to) reject(new Error(`From is superior at to`));
				if (to < from) reject(new Error(`to is superior to From`));

				const roomsWithoutBookings = rooms.filter(room => room.reservations.length === 0);

				const roomsFree = rooms.filter(room => {
					if (room.reservations.length === 0) return false;

					const x = room.reservations.filter(
						re => {
							const reFrom = new Date(re.from).getTime();
							const reTo = new Date(re.to).getTime();
							if (from === reFrom && to === reTo) return false;
							if (from > reFrom && from < reTo) return false;
							if (reFrom > from && reFrom < to) return false;
							// noinspection RedundantIfStatementJS
							if (reTo > from && reTo < to) return false;

							return true;
						});

					return room.reservations.length !== 0
					       && x.length;
				});
				resolve([...roomsWithoutBookings, ...roomsFree]);

			} catch (e) {
				console.log({ error: e });
				reject(e);
			}
		});
	}

	public async find(id: string): Promise<IRoom> {
		const room = await Room.findById(id).exec();

		if (!room) throw new Error(`Room with id '${ id }' not found'`);

		return room;
	}

	public create(room: IRoom): Promise<IRoom> {
		const newRoom = new Room(room);
		return newRoom.save();
	}

	public async update(id: string, room: IRoom): Promise<IRoom> {
		const updatedRoom = await Room.findByIdAndUpdate(id, room).exec();

		if (!updatedRoom) throw new Error(`Room with id '${ id }' not found'`);

		return updatedRoom;
	}

	public async delete(id: string): Promise<IRoom> {
		const deletedRoom = await Room.findByIdAndDelete(id).exec();

		if (!deletedRoom) throw new Error(`Room with id '${ id }' not found'`);

		return deletedRoom;
	}


}