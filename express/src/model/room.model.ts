import { Document, model, Schema } from 'mongoose';

export interface IReservation {
	from: Date
	to: Date
}

export interface IRoom extends Document {
	name: string;
	description: string;
	capacity: number;
	equipements: Object[];
	reservations: IReservation[],
	createdAt: Date;
	updatedAt: Date;
}

const RoomSchema = new Schema({
	name: { type: String, required: [true, 'Field is required'] },
	description: { type: String, required: [true, 'Field is required'] },
	capacity: { type: Number, min: 1, required: [true, 'Field is required'] },
	equipements: { type: [Object], required: [true, 'Field is required'] },
	reservations: { type: [Object] },
	createdAt: { type: Date, default: Date.now },
	updatedAt: { type: Date },
});

export const Room = model<IRoom>('Room', RoomSchema);